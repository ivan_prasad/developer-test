# Applicant Test for Developer at Nothing But Web
1. Please fork this repository and add your answers and code for the first section to your fork.
1. Fork the JSFiddle in section on and provide the link to the fork you did the challenges on in your Bitbucket fork.
1. Notify us of your fork with the completed challenges via a Bitbucket message to nothingbutweb.

If you are unable to answer all questions and complete all challenges by the deadline, please send us whatever you have.
Not having completed everything does not automatically rule you out for an interview.

## Modeling Questions / Backend Implementation
1. Imagine you have a project comprised of consecutive tasks A, B, C and D. Any task in the project will only become active when the preceding task has been completed, i.e. task B is inactive until task A has been has been completed, task C only becomes active on completion of B and so on. The data model should also be able to handle insertion, removal or swapping of tasks. How would you model this? What kind of objects  would you use or how would you lay out your database?
1. Your project workflow has changed, so that every task becomes a two step process where every task first needs to be assigned to someone from a group of users before it can be completed. How do you change your model to allow for this new workflow?
1. The assignee of task B has decided that he needs to create two new consecutive tasks E & F which he or she assigns to other users. He can only complete task B once these two tasks have been completed. The sub tasks should only ever be visible to the assignees of tasks B, E & F, i.e. the sub-tasks are fully transparent to all other participants of the main project. How would you adjust your model to achieve that?
1. Based on the model you described in 1-3, implement your solution in Python, PHP or Node.JS (JavaScript/CoffeeScript) using any framework and/or libraries of your choice. The implementation should also allow for the creation and display of the ordered tasks mentioned in 1 along with their status.
1. On completion of task C, a new project with consecutive tasks G, H & I is created which runs concurrently with the remainder of the current project (i.e. task D). How do allow for this? You do not need to implement this in actual code.
1. How would you report on details like current status of project or assignee of current task? Describe your approach or write a pseudo query.
1. How would you test your workflow representation? What would you test and how? You do not need to implement this in actual code.

## Frontend Challenges
Please fork from [this JSFiddle](http://jsfiddle.net/9r4378as/4/) to complete the following challenges.

1. Modify the block with class challenge_1 to show the three divs as columns where the left and right box are fixed width 100px and the middle column occupies all remaining space in between.
1. Modify the block with class challenge_2 to style all list elements as follows:
	1. Items in series 1, 4, 7, 10,... should be bold
	1. Items in series 5, 10, 15,... should be italic
1. Use the URL provided in the block with challenge 3 to retrieve the visitor's IP address and display it in the span with ID my_ip_address.